---
layout: post
title:  "The Sunday Read - Sigrid Johnson was black. A DNA test said she wasn't."
date:   2021-03-01 00:00:00 +0000
categories: [media]
---

> I am the product of someone, but the reflection of no one.