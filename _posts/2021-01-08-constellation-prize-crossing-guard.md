---
layout: post
title:  "Bianca Giaever - Constellation Prize : Crossing Guard"
date:   2021-01-08 00:00:00 +0000
categories: [media]
---

> "My time with Sophia had helped my loneliness, but it had not solved it. I had no priest, no shaman to go to with my problem. So I went to the contemporary equivalent: my doctor. When I told her about my loneliness she prescribed anti-depressants. I wanted to tell her that I was not depressed, I was lonely. I was lonely because I lived thousands of miles away from my parents, because I grew up thousands of miles away from my grandparents, because my mom was an immigrant, we were always reaching across different cultures. I wanted to tell her I was lonely because I was beholden of no one. Nothing was expected of me. I had no responsibilities other than my own material well being (which meant that I fought with my roommates about money). I wanted to tell her that I lived a life separate from the rhythms of nature and I wasn't born into any belief system -- that moving my car for street sweeping had become a cherished ritual in my life. But most of all, I wanted to tell her that my loneliness was beyond the physical and into the spiritual realm. I longed for ceremony, for stories and mythology to guide me. Had I unconsciously began looking in churches to feel this void? But I didn't say that to the doctor. I took the prescription and I filled it, and the loneliness was dulled."
