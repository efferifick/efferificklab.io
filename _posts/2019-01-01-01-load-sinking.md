---
layout: post
title:  "Load Sinking"
date:   2019-01-01 15:32:14 -0300
categories: [dictionary]
---

This is an optimization also known as partial dead-code elimination. Its purpose is to move expensive expressions closer to where they are actually used, in the hope that they are never executed if pushed under an if condition or if the function exits earlier.

Before load sinking:

```
x = * p;
if (condition) {
  return -1;
}
use(x);
```

After load sinking:

```
if (condition) {
  return -1;
}
x = * p; // Delay load *p. 
use(x);
```

https://devblogs.microsoft.com/cppblog/msvc-code-optimizer-improvements-in-visual-studio-2017-versions-15-5-and-15-3/