---
layout: post
title:  "Class hierarchy"
date:   2019-01-01 15:32:14 -0300
categories: [dictionary]
---

A class hierarchy is a set of classes order in a lattice created by derivation.
We use class hierarchies to represent concepts that have hierarchical relationships, such as “A fire engine is a kind of a truck which is a kind of a vehicle” and “A smiley face is a kind of a circle which is a kind of a shape.”
Huge hierarchies, with hundreds of classes, that are both deep and wide are common. 

\[0] Dean, Jeffrey, David Grove, and Craig Chambers. "Optimization of object-oriented programs using static class hierarchy analysis." European Conference on Object-Oriented Programming. Springer, Berlin, Heidelberg, 1995.