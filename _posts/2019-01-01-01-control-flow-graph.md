---
layout: post
title:  "Control Flow Graph"
date:   2019-01-01 15:32:14 -0300
categories: [dictionary]
---


"The usual representation for the control flow relationship of a program; the control conditions on which an operation depends can be derived from such a graph.”[0]

“A control flow graph is a directed graph G augmented with a unique entry node START and a unique exit node STOP such that each node in the graph has at most two successors.”[0]

[0] Ferrante, Jeanne, Karl J. Ottenstein, and Joe D. Warren. "The program dependence graph and its use in optimization." ACM Transactions on Programming Languages and Systems (TOPLAS) 9.3 (1987): 319-349.