---
layout: post
title:  "Points-to Analysis"
date:   2021-02-07 15:32:14 -0300
categories: [dictionary]
---

Points-to analysis computes an abstract model of the memory that is used to answer the following query: _What can a pointer variable point-to. i.e., what can its value be when dereferenced during program execution?_ \[0\]

Pointer analysis is a fundamental static program analysis, with a rich literature and wide applications. The goal of pointer analysis is to compute an approximation of the set of program objects that a pointer variable or expression can refer to.  \[1\]


[0] Balatsouras, George, and Yannis Smaragdakis. "Structure-sensitive points-to analysis for C and C++." International Static Analysis Symposium. Springer, Berlin, Heidelberg, 2016.

[1] Smaragdakis, Yannis, and George Balatsouras. "Pointer analysis." Foundations and Trends in Programming Languages 2.1 (2015): 1-69.
