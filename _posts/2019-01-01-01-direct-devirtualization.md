---
layout: post
title:  "Direct Devirtualization"
date:   2019-01-01 15:32:14 -0300
categories: [dictionary]
---

To devirtualize a dynamic method call without generating a guard test.\[0]

Criticisms: 
* Transforming a dynamic call to a static one does not make a tangible performance gain (owing to the low overhead of accessing the method table) unless it is inlined.\[0]
* Static analyses that attempt direct devirtualization work on the closed-world assumption, in which no dynamic class loading is allowed. Therefore, these techniques cannot be directly applicable to Java. Dynamic recompilation can be used to make direct devirtualization possible in the non-closed world assumption, but it involves a complicated mechanism called on-stack replacement.\[0]

\[0] Ishizaki, Kazuaki, et al. "A study of devirtualization techniques for a Java Just-In-Time compiler." ACM SIGPLAN Notices. Vol. 35. No. 10. ACM, 2000.