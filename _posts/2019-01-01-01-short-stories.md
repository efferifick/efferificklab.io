---
layout: post
title:  "Short Stories"
date:   2019-01-01 15:32:14 -0300
categories: [list]
---

* - King Alfred and the Cakes
* - King Alfred and the Beggar
* - King Canute on the Seashore
* - King John and the Abbot
* - The Sons of William the Conqueror
* - The White Ship
* Babel, Isaac - You must know everything [\[0\]](https://www.newyorker.com/podcast/fiction/george-saunders-reads-isaac-babel)
* DeLillo, Don - Baader Meinhoff [\[0\]](https://www.newyorker.com/podcast/fiction/chang-rae-lee-reads-don-delillo)
* DeLillo, Don - The Itch [\[0\]](https://www.newyorker.com/podcast/fiction/joy-williams-reads-don-delillo)
* Kafka, Franz - A Hunger Artist [\[0\]](https://www.theguardian.com/books/audio/2012/dec/28/hanif-kureishi-franz-kafka-hunger)
* Kafka, Franz - A Little Fable [\[0\]](https://www.youtube.com/watch?v=SzEO0qFFzwI)
* Kafka, Franz - Before the Law