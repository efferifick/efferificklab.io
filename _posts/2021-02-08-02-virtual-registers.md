---
layout: post
title:  "Virtual Registers"
date:   2021-02-08 00:00:01 -0300
categories: [dictionary]
---

Instead of a fixed set of registers, an infinite set of temporaries, called _virtual registers_ is normally used before register allocation pass. At the register allocation phase, some of the virtual registers will be replaced by physical registers while te rest will be spilled to memory. (Paraphrased from \[0\])

\[0\] Balatsouras, George, and Yannis Smaragdakis. "Structure-sensitive points-to analysis for C and C++." International Static Analysis Symposium. Springer, Berlin, Heidelberg, 2016.
