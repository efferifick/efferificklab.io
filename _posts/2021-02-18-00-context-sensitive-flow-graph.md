---
layout: post
title:  "Context Sensitive Control Flow Graph"
date:   2021-02-18 00:00:00 +0000
categories: [dictionary]
---

"We propose a way of constructing control flow graph, in which the graph only shows information that is relevant to users’ interests, and compressing the other information so that it is still available to users upon request. We let users decide what are the program points in the source code they are interested in, and build the interprocedural control flow graph that captures the program points. Other nodes in the control flow graph get collapsed into block nodes according to certain rules." 

**NOTE** It seems to me that the output is equivalent to an interprocedural control flow graph after slicing. However, since the CSCFG keeps the slicing criteria in a data-structure, it is re-usable, unlike the ICFG (after slicing). I'll need to read more about this.

\[0\] Ng, Jim-Carl. "Context-sensitive control flow graph." (2004).
