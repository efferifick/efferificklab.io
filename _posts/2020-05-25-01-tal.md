---
layout: post
title:  "This American Life - Time Out"
date:   2020-05-25 00:00:00 +0000
categories: [media]
---

> I thought it was a little bit weird that I would ask somebody for their used shoes but we had other clothes that were used from other people and, you know, she was tough and she kind of had this like you know, she seemed like she'd be the kind of person that would keep something like that to her and would know what it meant. And she did, she never told anybody. Except of course the most important person that she shouldn't have told: her mother. And her mother got really upset and told her that she couldn't give them to me. And there they were, hanging up on the telephone wire, my sneakers.

> And I'm sure her mom did that because she was afraid that if I came home with a pair of used sneakers that it would be insulting to my mother. I'm sure that they all had this understanding of themselves like you know you don't insult one of your peers by giving their kids your crappy shoes even though they are better than the crappy shoes they bought for them, you know?

> But there were always  really beautiful sneakers hanging up on the wires and there was no way to get them. No way at all. They just like floated up there. And you know what, I think that's when I decided the platonic idealism was true. There really was a perfect thing I would never experience, at least while I play on that Catholic league. 
