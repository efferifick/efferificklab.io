---
layout: post
title:  "Continous integration"
date:   2019-01-01 15:32:14 -0300
categories: [dictionary]
---

The process of automatically and continuously building a project from a version management repository. [0]

A simple technique to improve the quality of the software development process where an automated system continuously or periodically checks out the source code of a project, builds it, runs tests, and produces reports for the developers. Thus various errors that might accidentally be committed into the code base are automatically caught. Such a system allows more in-depth testing than what developers could feasibly do manually [0]

[0] Dolstra, Eelco, and Eelco Visser. "Hydra: A Declarative Approach to Continuous Integration."