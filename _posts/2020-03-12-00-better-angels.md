---
layout: post
title:  "Better Angels of Our Nature - Steven Pinker"
date:   2021-03-12 00:00:00 +0000
categories: [book]
---

> We never eat cookies cause cookies have yeast, and one little bite turns a man to a beast.

> How can you be a Mensch without a Zager?

> Maybe the time has even come when I can use a knife to push peas onto my fork.
