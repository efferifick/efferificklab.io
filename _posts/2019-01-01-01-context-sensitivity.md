---
layout: post
title:  "Context-sensitivity"
date:   2019-01-01 15:32:14 -0300
categories: [dictionary]
---


In the domain of intraprocedural analysis, a precise analysis is one whose solution is equivalent to the “meet-over-all-paths” solution. On the other hand, a precise interprocedural analysis is one whose solution is equivalent to the “meet-over-all-valid-paths” solution. A path is valid if whenever the end of a procedure is reached along the path, control is returned to the site of the most recent call. The ability of an analysis to disregard invalid paths is often referred to as context sensitivity. [0]

[0] Reps, Thomas, Susan Horwitz, and Mooly Sagiv. "Precise interprocedural dataflow analysis via graph reachability." Proceedings of the 22nd ACM SIGPLAN-SIGACT symposium on Principles of programming languages. ACM, 1995.