---
layout: post
title:  "Partial SSA form"
date:   2021-02-08 00:00:01 -0300
categories: [dictionary]
---

Program variables are divided into two categories.

1. variables whose address is taken and can be referenced by pointers.
2. variables that can never be referenced by pointers.

The latter are converted to SSA, whereas the former are kept in memory by using (i) `alloca` instructions to allocate the required space on stack, and (ii) load/store instructions to access or update, respectively, the variable contents, at any point (hence escaping SSA form). This technique has been termed _"partial-SSA"_

\[0\] Balatsouras, George, and Yannis Smaragdakis. "Structure-sensitive points-to analysis for C and C++." International Static Analysis Symposium. Springer, Berlin, Heidelberg, 2016.
