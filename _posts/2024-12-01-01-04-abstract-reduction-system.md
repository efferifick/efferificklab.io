---
layout: post
title:  "Abstract reduction system"
date:   2024-12-01 00:00:01 -0300
categories: [dictionary]
---

**Abstract reduction system**: is a pair <math xmlns="http://www.w3.org/1998/Math/MathML">
  <mfenced open="(" close=")">
    <mrow>
      <mi>A</mi>
      <mo>,</mo>
      <mo>→</mo>
    </mrow>
  </mfenced>
</math>, where the reduction <math xmlns="http://www.w3.org/1998/Math/MathML">
  <mo>→</mo>
</math> is a binary relation on the set <math xmlns="http://www.w3.org/1998/Math/MathML"><mi>A</mi></math>, i.e., <math xmlns="http://www.w3.org/1998/Math/MathML">
  <mrow>
    <mo>→</mo>
    <mo>⊆</mo>
    <mi>A</mi>
    <mo>×</mo>
    <mi>A</mi>
  </mrow>
</math>. Instead of <math xmlns="http://www.w3.org/1998/Math/MathML">
  <mfenced open="(" close=")">
    <mrow>
      <mi>a</mi>
      <mo>,</mo>
      <mi>b</mi>
    </mrow>
  </mfenced>
  <mo>∈</mo>
  <mo>→</mo>
</math> we write <math xmlns="http://www.w3.org/1998/Math/MathML">
  <mrow>
    <mi>a</mi>
    <mo>→</mo>
    <mi>b</mi>
  </mrow>
</math>.

Baader, Franz, and Tobias Nipkow. Term rewriting and all that. Cambridge university press, 1998.