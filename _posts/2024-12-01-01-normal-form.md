---
layout: post
title:  "Termination"
date:   2024-12-01 00:00:01 -0300
categories: [dictionary]
---

**Termination**. In the context of term rewriting, termination is when after a normal form is reached after finitely many rule applications.

Baader, Franz, and Tobias Nipkow. Term rewriting and all that. Cambridge university press, 1998.