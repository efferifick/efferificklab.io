---
layout: post
title:  "Confluence"
date:   2024-12-01 00:00:01 -0300
categories: [dictionary]
---

**Confluence**: In term rewriting, if we can find a common term <math xmlns="http://www.w3.org/1998/Math/MathML"><mi>s</mi></math> that can be reached both from <math xmlns="http://www.w3.org/1998/Math/MathML"><msub><mi>t</mi><mn>1</mn></msub></math> and <math xmlns="http://www.w3.org/1998/Math/MathML"><msub><mi>t</mi><mn>2</mn></msub></math> by rule application, then we say that terms <math xmlns="http://www.w3.org/1998/Math/MathML"><msub><mi>t</mi><mn>1</mn></msub></math> and <math xmlns="http://www.w3.org/1998/Math/MathML"><msub><mi>t</mi><mn>2</mn></msub></math> are confluent.

Baader, Franz, and Tobias Nipkow. Term rewriting and all that. Cambridge university press, 1998.