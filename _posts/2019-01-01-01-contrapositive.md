---
layout: post
title:  "Contrapositive"
date:   2019-01-01 15:32:14 -0300
categories: [dictionary]
---

If<math xmlns="http://www.w3.org/1998/Math/MathML" display="inline" alttext="upper P"><mi>P</mi></math>then<math xmlns="http://www.w3.org/1998/Math/MathML" display="inline" alttext="upper Q"><mi>Q</mi></math> <math xmlns="http://www.w3.org/1998/Math/MathML" display="inline" alttext="right double arrow"><mo stretchy="false">⇒<!-- ⇒ --></mo></math>if not <math xmlns="http://www.w3.org/1998/Math/MathML" display="inline" alttext="upper Q"><mi>Q</mi></math>then not<math xmlns="http://www.w3.org/1998/Math/MathML" display="inline" alttext="upper P"><mi>P</mi></math>.

Harrison, John. Handbook of practical logic and automated reasoning. Cambridge University Press, 2009.