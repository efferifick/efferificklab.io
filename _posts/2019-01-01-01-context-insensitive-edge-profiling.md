---
layout: post
title:  "Context-insensitive edge profiling"
date:   2019-01-01 15:32:14 -0300
categories: [dictionary]
---

"Profile information collected at runtime at the granularity of a single call edge.” [0]

[0] Hazelwood, Kim, and David Grove. "Adaptive online context-sensitive inlining." Code Generation and Optimization, 2003. CGO 2003. International Symposium on. IEEE, 2003.