---
layout: post
title:  "Maximal Fixed Point"
date:   2019-01-01 15:32:14 -0300
categories: [dictionary]
---

Historically, the least solution to the data flow equations is called the MFP solution (for Maximal Fixed Point) althout it in fact computes the least fixed point; the reason is that the classical literature tends to focus on analyses where <math xmlns="http://www.w3.org/1998/Math/MathML" display="inline" alttext="square-cup"> <mo>⊔<!-- ⊔ --></mo></math> is <math xmlns="http://www.w3.org/1998/Math/MathML" display="inline" alttext="intersection"><mo>∩<!-- ∩ --></mo></math> (and because the least fixed point with respect to <math xmlns="http://www.w3.org/1998/Math/MathML" display="inline" alttext="square-image-of-or-equal-to"><mo>⊑<!-- ⊑ --></mo></math> or <math xmlns="http://www.w3.org/1998/Math/MathML" display="inline" alttext="superset-of-or-equal-to"><mo>⊇<!-- ⊇ --></mo></math> then equals the greatest fixed point with respect to <math xmlns="http://www.w3.org/1998/Math/MathML" display="inline" alttext="subset-of-or-equal-to"><mo>⊆<!-- ⊆ --></mo></math>)

Nielson, Flemming, Hanne R. Nielson, and Chris Hankin. Principles of program analysis. Springer, 2015.

