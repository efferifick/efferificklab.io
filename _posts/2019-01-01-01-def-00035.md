---
layout: post
title:  "Class Test"
date:   2019-01-01 15:32:14 -0300
categories: [dictionary]
---

```
r0 := <receiver object>
r1 := load(r0 + <offset-of-class-in-object>)
if (r1 == <address-of-expected-class>) {
  <method inlining>}
else {
  r2 := load(r1 + <offset-of-method-in-class>);
  call r2
}
```

\[0] Ishizaki, Kazuaki, et al. "A study of devirtualization techniques for a Java Just-In-Time compiler." ACM SIGPLAN Notices. Vol. 35. No. 10. ACM, 2000.