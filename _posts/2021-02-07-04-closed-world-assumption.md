---
layout: post
title:  "Closed World Assumption"
date:   2021-02-07 00:00:01 -0300
categories: [dictionary]
---

In Java, the closed world assumption is used to limit the scope of static analysis to reasonable constraints. The assumption is as follows:

> We assume that only classes reachable from the class path at analysis time can be used by the application at runtime.

This assumption is not true generally speaking because class loaders can access network information or other runtime information to reach more classes.

\[0\]

\[0\] Livshits, Benjamin, John Whaley, and Monica S. Lam. "Reflection analysis for Java." Asian Symposium on Programming Languages and Systems. Springer, Berlin, Heidelberg, 2005.
