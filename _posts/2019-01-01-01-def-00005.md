---
layout: post
title:  "Acceptance Tests"
date:   2019-01-01 15:32:14 -0300
categories: [dictionary]
---

Tests designed to simulate real-world users as they interact with the system. Ideally, they are automated and written as a narrative of sorts.

\[0] Carter, Joshua D., and Susannah D. Pfalzer. New programmer's survival manual : navigate your workplace, cube farm, or startup. Place of publication not identified: Pragmatic Bookshelf, 2011. Print.