---
layout: post
title:  "Security Guide for Family"
date:   2020-04-19 00:00:00 +0000
categories: [list]
---

# Security Guide for Family

This is a document I am preparing for my family.
This document is about computer security.
I will write about:

1. Proactive methods to protect against attacks.
2. Reactive methods to protect after attacks.

Before we start, I want to say the following:
Most of the burden and inconvienience of being secure comes from the reactive methods.
You cannot control when you are going to be attacked, nor what attack is performed.
You could be on a vacation, in the middle of a pandemic, or at work.
If an attack happens, you need to react, and you will need to make sure that your accounts are secured and all of this takes time and effort.
If you are proactive, then you will ensure **during your free time** that if an attack happens, it is not as damaging as it could be.
Therefore, being proactive allows you reduce the amount of uncertainty during and after the attack.

Some of these attacks are obvious and I am sure you are protecting yourself already against them.
However, it helps to be explicit about them to see how if one attack happens, different proactive measures will protect you.

## Attack \#0: Protect against snooping

Let's assume that you have an attacker that is interested in some data that you have in your device.
Let's assume:
* <span style="color:red">**the attacker has unsupervised physical access**</span> to your devices
* <span style="color:red">**your device is on**</span>
* <span style="color:red">**your account is logged in**</span>
* <span style="color:green">**the attacker has very little time**</span>
* <span style="color:green">**the attacker has no ability to take your device away**</span>

Who is most likely to perform this attack? Loved one, co-workers, people you know.

Will the attacker be able to obtain the data from your machine? Yes, definitely.

How will the attacker obtain the data? Just using the computer.

Will you be able to know if an attacker accessed your data? No. 


How can we proactively protect ourselves? Never let your devices logged in and use a password for your account.

* **Advice #0**: Always have a login password that is secret on your computer account.

## Attack \#1: Protect against professional snooping

Let's change our assumptions, because you've learned not to leave an account logged in unsupervised. However, you have to leave your computer unsupervised for longer.

* <span style="color:red">**the attacker has unsupervised physical access**</span> to your devices
* <span style="color:red">**the attacker has a several hours of time**</span>
* <span style="color:red">**the attacker has the ability to tear apart your machine and rebuild it**</span>
* <span style="color:green">**your account is logged out**</span>
* <span style="color:green">**the attacker has no ability to take your device away**</span>

Who is most likely to perform this attack? Potentially but unlikely, a co-worker with medium IT skills that is trying to snoop on the machines you leave at work over night.

Will the attacker be able to obtain the data from your machine? Yes, definitely.

How will the attacker obtain the data? The easiest way to do this is to take the hardrive from the computer and clone it. Yes, your account is "password protected" but that doesn't mean it is encrypted. Once your data is on the external hard drive, they can easily access your account data.

Will you be able to know if an attacker accessed your data? No.

How can we proactively protect ouselves? Always encrypt your data with a strong password.

* **Advice #1**: Always encrypt your devices. If you are not sure if your devices are encrypted, let me know and I can help you find out.

## Spy level snooping

Is it still possible that someone has access to your data after

* <span style="color:green">**your account is logged out**</span>, AND
* <span style="color:green">**your device is encrypted**</span>?

Yes, this depends on several factors, whether

* <span style="color:red">**your device is on**</span>, OR
* <span style="color:red">**your attacker is a nation**</span>

can lead to several other vectors of attack. However, these are highly unlikely and you shouldn't worry about these (too much). If you want to protect yourself from these attacks, turn off your computer and stay there a couple of minutes and also use strong passwords such that it takes a long long time to decrypt your computer.

## Attack \#2: Theft

Let's change our assumptions. The attacker doesn't care to remain undetected and wants your physical computer.

* <span style="color:green">**your account is logged out**</span>
* <span style="color:green">**your device is encrypted**</span>
* <span style="color:red">**the attacker has the ability to take your device away**</span>

All snooping attacks are still possible, so a log-in password and encryption are still recommended.

Who is most likely to perform this attack? Thieves

Will the attacker be able to obtain the data from your machine? With encryption and log in passwords it will take time.

Will the attacker be able to sell your machine and thus profit? Yes, definitely.

What will it cost us? Monetary value of the laptop and losing any data that was not backed up.

How can we react to such an attack? Buying a new laptop, filing insurance claims (if applicable).

Can we recover data that was not backed up? No.

How can we proactively protect ourselves from such an attack? You can use a laptop lock to make it impossible that a thief can still your laptop. Perhaps, most importantly you should make backups so that any data that is not on the cloud can be recovered. You can make your device less attractive to steal by buying old devices and laser sketching them. Also, you should activate the "find my laptop" feature as this will make it more likely (albeit still unlikely ) that you recover your laptop. (Let's cross fingers our attackers are dumb!)

* **Advice #2**: Perform backups regularly. Please encrypt these backups too.<sup>I need to research for a family friendly way to perform backups</sup>
* **Advice #3**: Activate find my laptop.
* **Advice #4**: Write down your laptop's serial number and store it in a secure location. You can help police by providing more evidence and ways to identify your laptop.
* **Advice #5**: Macs and other computers have a boot password. Use a boot password.<sup>Ask me what it is if you are curious</sup>. This will make sellers of stolen goods have a harder time clearing your encrypted information from disk and therefore bother them. The buyer of a stolen laptop will also notice something is wrong when they try to update the operating system so your laptop will be more likely to be returned.
* <span style="color:blue">**Semi-Advice #0**:</span> Make your laptop easy to identify by laser engraving it. Take a photo and show it to the police.

## Attack \#3: Malware

* <span style="color:green">**your account is password protected**</span>
* <span style="color:green">**your device is encrypted**</span>
* <span style="color:red">**the attacker has the ability to ask you to install a malicious package**</span>

Unfortunately, none of the protections against theft and snooping really help if you install malware. Well... they can help, but it is a different level of encryption (file encryption vs disk encryption)...

Who is most likely to perform this attack? Android apps. iOS apps to a lesser extent, but still very much possible.

What will the atacker gain? Depending on the level of sophistication, they can put ads on your machine or they can take over full control of your machine or they can make it impossible for you to work by putting pop-ups every second.

How can you react? If you do install malware, you can do a factory reset of your machine or run some antivirus software.

How can you be proactive? Do not install anything that you don't trust or that you don't need. If you want to try an application out and you don't know if you should trust it, then there are ways to run this application securely, but it depends on the context. <sup>It is better to not install anything, but if you need to set up a virtual machine let me know and we can contain your installation somehow without access to most of your machine</sup>

* **Advice #6**: Only install on your computer things you **need** and **trust**. If you need, but don't trust something, then we can work on installing it on a container (or on a different machine...)

## Attack \#4: Peer attacks

* <span style="color:green">**your account is password protected**</span>
* <span style="color:green">**your device is encrypted**</span>
* <span style="color:green">**you only install needed and trusted applications**</span>
* <span style="color:red">**the attacker is a peer on a network you trust**</span>

Who is most likely to perform this attack? Some script kiddie on a public starbucks wifi.<sup>I think...</sup>

What can the attacker gain from this attack? They can misrepresent themselves as you on the unprotected websites you visit while using the public network.

How can we know if we have been victim of a Man in the Middle Attack? Not sure...

How can we react? If we do know that we have been misrepresented, then we should change passwords on the accounts which we believe were compromised.

How likely is this attack? Not much thanks to new internet technologies <sup>I think...</sup>

What can we do to protect ourselves proactively?

* **Advice #7** Install HTTPS everywhere
* **Advice #8** Only visit websites that use HTTPS (this is most websites)

## Attack \#4: Network attacks

* <span style="color:green">**your account is password protected**</span>
* <span style="color:green">**your device is encrypted**</span>
* <span style="color:green">**you only install needed and trusted applications**</span>
* <span style="color:green">**you only visit trusted websites**</span>
* <span style="color:red">**the attacker controls the network you are working on**</span>

Who is most likely to perform this attack? Anyone with a free wifi that seems suspicious. More likely in tourist areas, hotels, and convention centres.

What kind of attack can they perform? They can see which web pages you are contacting and misrepresent themselves as you. 

What can the attacker gain from this attack? They can misrepresent themselves as you on the unprotected websites you visit while using the public network.

How can we know if we have been victim of a Man in the Middle Attack? Not sure...

How can we react? If we do know that we have been misrepresented, then we should change passwords on the accounts which we believe were compromised.

How likely is this attack? Not much thanks to new internet technologies <sup>I think...</sup>

What can we do to protect ourselves proactively?

* **Advice #9** Only use networks you trust (4G, 5G, home wifi, work wifi)
* **Advice #10** If you must use a network you don't know if you can trust, then use a VPN (a VPN you trust!)

## Attack \#5: Protecting against data dumps

* <span style="color:green">**your account is password protected**</span>
* <span style="color:green">**your device is encrypted**</span>
* <span style="color:green">**you only install needed and trusted applications**</span>
* <span style="color:green">**you only visit trusted websites**</span>
* <span style="color:green">**you only join trusted networks**</span>
* <span style="color:red">**there's been a data dump with your info**</span>

