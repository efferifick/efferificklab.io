---
layout: post
title:  "Isaac Asimov - Nightfall II"
date:   2020-09-25 00:00:00 +0000
categories: [book]
---

> Dr, Sloane said, with casual suddenness, 'Would you like to take a walk with me, Richard?'
>
> The boy's eyes widened and he stopped wriggling.
> He looked directly at Dr. Sloane.
> 'A walk, sir?'
>
> 'I mean, outside.'
>
> 'Do you go -- outside?'
>
> 'Sometimes. When I feel like it.'
>
> Richard was on his feet, holding down a squirming eagerness.
> 'I didn't think anyone did.'
>
> 'I do.
> And I like company.'
>
> The boy sat down, uncertainly.
> 'Mom?--'
>
> Mrs. Hanshaw had stiffened in her seat, her compressed lips radiating horror, but she managed to say, 'Why certainly Dickie.
> But watch yourself.'
>
> And she managed a quick and baleful glare at Dr. Sloane.
>
>
>
> In one respect, Dr. Sloane had lied.
> He did _not_ go outside 'sometimes'.
> He hadn't been in the open since early college days. [...]
>
> So there was a crawling sensation about his skin when he felt wind touch it, and he put down his flexied shoes on bare grass with a gingerly movement.


> Look at these flowers. They are the kind that smell.


> 'Won't he ever be normal again?'
>
> Dr. Sloane rose to his feet.
> 'Mrs. Hanshaw, he's as normal as need be right now.
> Right now, he's tasting the joys of the forbidden.
> If you co-operate with him, show that you don't disapprove, it will lose some of its attraction right there.
> Then, as he grows older, he will become more aware of the expectations and demands of society.
> He will learn to conform.
> After all, there is a little of the rebel in all of us, but it generally dies down as we grow old and tired.
> Unless, that is, it is unreasonably suppressed and allowed to build up pressure.
> Don't do that.
> Richard will be all right.
>
> He walked to the Door.
>
> Mrs Hanshaw said, 'And you don't think a probe will be necessary, doctor?'
>
> He turned and said vehemently, 'No, definitely not!
> There is nothing about the boy that requires it.
> Understand?
> Nothing.'
>
> His fingers hesitated an inch from the combination board and the expression on his face grew lowering.
>
> 'What's the matter, Dr. Sloane?' asked Mrs. Hanshaw.
>
> But he didn't hear her because he was thinking of the Door and the psychic probe and all the rising, choking tide of machinery.
> There is a little of the rebel in all of us, he thought.
>
> So he said in a soft voice, as his hand fell away from the board and his feet turned away from the Door, 'You know, it's such a beautiful day that I think I'll walk.;
