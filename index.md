---
layout: default
---

  {% include header.html %}
  
<div id="hcard-Erick-Ochoa" class="vcard" style="display: none;">
 <span class="fn">Erick Ochoa</span>
 <a class="email" href="mailto:eochoa@ualberta.ca">eochoa@ualberta.ca</a>
 <a class="u-url" rel="me">ceci-nest-pas.me</a>
 <p class="p-note">Compiler Engineer</p>
</div>

# About

My name is Erick Ochoa.

You can send me an e-mail to this address: <a rel="me" href="mailto:eochoa@ualberta.ca">eochoa@ualberta.ca</a>.

I own <a rel="me" href="https://github.com/efferifick/">this github account.</a>

I own this [google scholar account](https://scholar.google.com/citations?user=V7AK8RUAAAAJ&hl).

These are [my contributions to wikipedia](https://en.wikipedia.org/wiki/Special:Contributions/Erickomoto).

# Detailed footprint

The github user [livibetter](https://github.com/livibetter) found a small program I made, named [wave](https://github.com/efferifick/wave), useful to generate sounds for alarms. They were kind enough to [submit pull requests](https://github.com/efferifick/wave/pulls?q=is%3Apr+is%3Aclosed), write a [blog post](https://web.archive.org/web/20190726191812/http://yjlv.blogspot.com/2015/01/wave-command-generates-sine-wave.html), and film a [video demo](https://www.youtube.com/watch?v=xgoW4BzQEtk).

The maintainer CelestialWalrus submitted my program wave to the [arch-linux user repository](https://aur.archlinux.org/packages/wave-git/). I've doubt many people have downloaded it, but it was a nice surprise to see this.

The [OMR](https://github.com/eclipse/omr)/[OpenJ9](https://github.com/eclipse/openj9) repositories have merged some of my [pull](https://github.com/eclipse/openj9/pulls?utf8=%E2%9C%93&q=is%3Apr+author%3Aefferifick+) [requests](https://github.com/eclipse/omr/pulls?utf8=%E2%9C%93&q=is%3Apr+author%3Aefferifick).

I have contributed a commit to [annobin's build system](https://sourceware.org/git/?p=annobin.git;a=search;s=Erick+Ochoa;st=author) and I hope to keep contributing more in the near future.

I try to submit bugs to [gcc whenever I can](https://gcc.gnu.org/bugzilla/buglist.cgi?emailcc2=1&email2=erick.ochoa%40theobroma-systems.com&bug_status=UNCONFIRMED&bug_status=NEW&bug_status=ASSIGNED&bug_status=SUSPENDED&bug_status=WAITING&bug_status=REOPENED&bug_status=RESOLVED&bug_status=VERIFIED&bug_status=CLOSED&emailtype2=substring&emailreporter2=1&cf_known_to_fail_type=allwords&query_format=advanced&cf_known_to_work_type=allwords&order=Importance) and I am an active participant in [gcc mailing list](https://gcc.gnu.org/cgi-bin/search.cgi?q=erick.ochoa%40theobroma-systems.com&cmd=Search&m=all&s=DRP)

[E Ochoa, C Müllner, P Tomsich. Soufflé, Points-To Analysis and Data Layout Optimizations. LPC 2021]({{site.baseurl}}/img/points-to.pdf)